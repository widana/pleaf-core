<?php
namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class CreateBasicBfByIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaf:create-basic-bf-index {package} {model} {index} {dir?} {author?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Basic Business Function By Id {location business object} {location model} {author}';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $package = $this->argument("package");
        $model = $this->argument("model");
        $dir = $this->argument("dir");
        $index = $this->argument("index");
        $author = $this->argument("author");

        $result = collect(config("leaf_command"))->where("name", $package)->first();

        if(is_null($result)){
            $this->error("Package $package not found");
            return;
        }

        $this->info("Process generate Basic BF");

        $outputDir = $result["dir"]["bo"];
        $exceptionDir = $result["constantException"];
        if(!is_null($dir)) {
            $outputDir = $outputDir."/".$dir;
        }

        $namespaceBo = $result["namespace"]["bo"];
        $namespace = $result["namespace"]["model"];

        $listOfFile = [
            "Find".$model."ByIndex",
            "Is".$model."ExistsByIndex",
            "Val".$model."ExistsByIndex",
        ];

        $someNamespaceModel = $namespace."\\".$model;
        $explodeIndex = explode(",", $index);

        $paramKeyValue = "";
        $condition = "";
        $indexExc = "";
        $indexParams = "";
        foreach ($explodeIndex as $key=>$value) {
            $key1 = lcfirst(str_replace(" ","", ucwords(str_replace("_", " ", $value))));
            $key2 = ucwords(str_replace("_", " ", $value));
            $paramKeyValue .= "\n\t\t"."$".$key1." = "."$"."dto"."['".$key1."'];";

            if($condition == "") {
                $condition .= "where('$value', $".$key1.")";
            } else {
                $condition .= "\n\t\t\t\t\t\t\t->where('$value', $".$key1.")";
            }

            $indexParams .= "$"."$key1, ";
            $indexExc .= " $key2 {".$key."}";
        }

        $input = [
            "var" => lcfirst($model),
            "model" => $model,
            "namespace" => $namespaceBo,
            "namespaceModel" => $someNamespaceModel,
            "author" => $author,
            "condition" => $condition."\n\t\t\t\t\t\t\t->first()",
            "paramKeyValue" => $paramKeyValue,
            "paramException" => substr_replace($indexParams ,"",-2)
        ];

        $findExcConstant = strtoupper($model)."_NOT_FOUND_WITH_INDEX";
        $findExcString = ucfirst($model)." not found with index,".$indexExc;
        $valExcConstant = strtoupper($model)."_DOES_NOT_EXISTS_WITH_INDEX";
        $valExcString = ucfirst($model)." does not exists with index,".$indexExc;

        $template = null;
        foreach ($listOfFile as $value) {

            if($value == "Find".$model."ByIndex") {
                $template = "pleaf-core::templates/create-basic-bf-find";
            } else if($value == "Is".$model."ExistsByIndex") {
                $template = "pleaf-core::templates/create-basic-bf-is";
            } else if($value == "Val".$model."ExistsByIndex") {
                $template = "pleaf-core::templates/create-basic-bf-val";
            }

            $input["className"] = $value;
            $input["findExcConstant"] = $findExcConstant;
            $input["findExcString"] = $findExcString;
            $input["valExcConstant"] = $valExcConstant;
            $input["valExcString"] = $valExcString;

            $view = view($template, $input);

            $path = $outputDir."/".$value.".php";

            if(!is_null($template)) {
                $this->generateFile($path, $view->render());
            }

            $this->info("Generated File: ". $path);

        }

        $explodeDirException = explode("/", $exceptionDir);
        $fileNameExc = array_splice($explodeDirException, count($explodeDirException)-1, 1);
        $isDirException = str_replace("/".$fileNameExc[0], "", $exceptionDir);

        if(!is_dir($isDirException)) {
            mkdir($isDirException);
            if(!file_exists($exceptionDir)) {
                $file = fopen($exceptionDir, "w");
                $textLine = "<?php\n";
                fwrite($file, $textLine);
                fclose($file);
            }
        }

        $path = fopen($exceptionDir, "a");
        $pathRead = fopen($exceptionDir, "r");
        $textData = fread($pathRead,filesize($exceptionDir));
        $text1 = "\nIF(!defined".'("'.$findExcConstant.'"))'."\n"."\tdefine".'("'.$findExcConstant.'"'.",".'"'.$findExcString.'");';
        $text2 = "\nIF(!defined".'("'.$valExcConstant.'"))'."\n"."\tdefine".'("'.$valExcConstant.'"'.",".'"'.$valExcString.'");';

        if (!self::contains($textData, $findExcConstant)) {
            fwrite($path, $text1);
        }

        if (!self::contains($textData, $valExcConstant)) {
            fwrite($path, $text2);
        }

        fclose($pathRead);
        fclose($path);

        $this->info("Done generate Basic BF");
    }

    private function generateFile ($path, $content){
        $f = fopen($path, "w");
        fwrite($f,"<?php\n\n");
        fwrite($f, $content);
        fclose($f);
    }

    private function contains($haystack, $needle, $caseSensitive = false) {
        return $caseSensitive ?
            (strpos($haystack, $needle) === FALSE ? FALSE : TRUE):
            (stripos($haystack, $needle) === FALSE ? FALSE : TRUE);
    }

}