<?php
/**
 * Created by PhpStorm.
 * @author: Widana <widananurazis@gmail.com>
 * Date: 06/04/16
 */

namespace Sts\PleafCore\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class CreatePackage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leaf:create-package {key}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a New Packages {location}.';

    /*
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $explode = explode("/", $this->argument("key"));
        $first = array_shift($explode);
        $last = end($explode);
//        $slice = explode("-",$last);
        \Log::debug(end($explode));
        \Log::debug(array_shift($explode));


        if(preg_match('/[^a-z\-0-9]/i',$last)){
            $this->error('name only input character');
        }else{
            $name1 = str_replace("-", "_", $last);
            $space = str_replace("-", " ", $last);
            $name2 = ucwords($space);
            $name2 = str_replace(" ", "", $name2);
            $name =    ucwords($first);
            $names = $name." ".$name2;
            $namespace = str_replace(" ","\\",$names);


//        \Log::debug(base_path());


            if(!is_dir(self::base_path("/packages"))) {
                mkdir(self::base_path("/packages"));
            }
            $parent_directory = self::base_path("/packages/".$first."/".$last);
            if(!is_dir($parent_directory)) {
                mkdir($parent_directory, 0755, true);
                $this->info('Package done generated!');

                $tmp = [
                    "tmp_loc_provider"=>$parent_directory."/Providers",
                    "tmp_loc_assets" => ($parent_directory."/assets"),
                    "tmp_loc_assets_js" => ($parent_directory."/assets/js"),
                    "tmp_loc_bo" => ($parent_directory."/BusinessObjects"),
                    "tmp_loc_cnf" => ($parent_directory."/config"),
                    "tmp_loc_ctrl" => ($parent_directory."/Controllers"),
                    "tmp_loc_mdl" => ($parent_directory."/Model"),
                    "tmp_loc_pbl" => ($parent_directory."/public"),
                    "tmp_loc_img" => ($parent_directory."/public/img"),
                    "tmp_loc_vws" => ($parent_directory."/views"),
                ];




                foreach ($tmp as $key => $value) {
                    mkdir($value);
                    $this->info("$value has successfully generated. !!!");
                }

                $tmp_templates = [[
                    "path" => ('' . $tmp["tmp_loc_provider"] . '/' . $name2.'Provider.php'),
                    "templates" => view("pleaf-core::templates/generate-new-package",
                        [
                            "name_package" => $last,
                            "name_class"=>$name2,
                            "name_config"=>$name1,
                            "namespace" => $namespace
                        ])
                ], [
                    "path" => ('' . $parent_directory . '/' . 'routes.php'),
                    "templates" => view("pleaf-core::templates/routes"),
                ]];
                $this->info("Name Package $last has successfully generated. !!!");



                if (count($tmp_templates) > 0) {
                    foreach ($tmp_templates as $key => $value) {
                        $this->generateFile($value["path"], $value["templates"]->render());
                        $this->info('' . $value["path"] . ' has successfully generated. !!!');
                    }
                }

//            $this->info("Please Added composer.json section autoload . !!!");
                if (count($tmp_templates) > 0) {
                    foreach ($tmp_templates as $key => $value) {
                        $this->generateFile($value["path"], $value["templates"]->render());
                        $this->info('' . $value["path"] . ' has successfully generated. !!!');
                    }
                }

            } else {
                $this->error('Package already generated!');
            }

            return;
        }
    }

    private static function base_path($path) {
        return base_path($path);
    }

    private function generateFile($path, $content)
    {
        $f = fopen($path, "w");
        fwrite($f, "<?php\n\n");
        fwrite($f, $content);
        fclose($f);
    }
}


