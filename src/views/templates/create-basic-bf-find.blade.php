namespace {{ $namespace }};

use Sts\PleafCore\BusinessFunction;
use Sts\PleafCore\DefaultBusinessFunction;
use Sts\PleafCore\CoreException;
use {{ $namespaceModel }};
use Log;

/**
 * @author {{ $author }}
 * @in
 *
 * @out
 */
class {{ $className }} extends DefaultBusinessFunction implements BusinessFunction {

    public function getDescription(){
    	return "{{$className}}";
    }

    public function process($dto){
        {!! $paramKeyValue !!}

   	    ${{ $var }} = {{ $model }}::{!! $condition !!};
    	
        if(is_null(${{ $var }})) {
            throw new CoreException({{$findExcConstant}}, [{{$paramException}}]);
        }

        return ${{ $var }};

    }

}