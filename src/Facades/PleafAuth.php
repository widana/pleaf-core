<?php
/**
 * Created by PhpStorm.
 * User: sts
 * Date: 20/02/18
 * Time: 10:07
 */

namespace Sts\PleafCore\Facades;

use Illuminate\Support\Facades\Facade;

class PleafAuth extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sts.pleaf.auth';
    }

}