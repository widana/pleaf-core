<?php

namespace Sts\PleafCore\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;
use Log;
use Sts\PleafCore\CoreException;

class PleafModel extends Model
{
    protected function performUpdate(Builder $query, array $options = [])
    {
        $dirty = $this->getDirty();

        $original = $this->getOriginal();
        Log::debug($original['version']);
        if (count($dirty) > 0) {
            if($original['version']!=$this->version){
                throw new CoreException(ERROR_DATA_CONCURRENCY_VALIDATION,[],[
                        'versionDataSystem'=>$original['version'],
                        'versionDataInput'=>$this->version
                    ]);
            }

            // If the updating event returns false, we will cancel the update operation so
            // developers can hook Validation systems into their models and cancel this
            // operation if the model does not pass validation. Otherwise, we update.
            if ($this->fireModelEvent('updating') === false) {
                return false;
            }

            // First we need to create a fresh query instance and touch the creation and
            // update timestamp on the model which are maintained by us for developer
            // convenience. Then we will just continue saving the model instances.
            if ($this->timestamps && Arr::get($options, 'timestamps', true)) {
                $this->updateTimestamps();
            }

            if(isset($this->version)){
                $this->version = $this->version+1;
                
            }

            // Once we have run the update operation, we will fire the "updated" event for
            // this model instance. This will allow developers to hook into these after
            // models are updated, giving them a chance to do any special processing.
            $dirty = $this->getDirty();

            if (count($dirty) > 0) {
                $numRows = $this->setKeysForSaveQuery($query)->update($dirty);

                $this->fireModelEvent('updated', false);
            }
        }

        return true;
    }
    
}