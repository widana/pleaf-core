<?php

namespace Sts\PleafCore\Util;

class Dto {

    public function fromDto($array, $case = "CAMEL") {
        $data = json_decode(json_encode($array), true);
        return $this->array_change_key_case($data, true, $case);
    }

    public function singleDto($array, $case = "CAMEL") {
        $data = json_decode(json_encode($array), true);

        if(is_null($data)) {
            return [];
        }

        if(sizeof($data)) {
            return $this->array_change_key_case($data, false, $case);
        } else {
            return [];
        }
    }

    private function array_change_key_case(&$array, $isMultipleRecord = false, $case = null)
    {
        $collection = [];

        if($isMultipleRecord) {
            foreach ($array as $item) {
                foreach ($item as $key=>$data) {
                    switch (strtoupper($case)) {
                        case 'UPPER':
                            $caseKey = strtoupper($key);
                            break;
                        case 'LOWER':
                            $caseKey = strtolower($key);
                            break;
                        case 'CAMEL':
                            $caseKey = camel_case($key);
                            break;
                        case 'SNAKE':
                            $caseKey = snake_case($key);
                            break;
                        case 'STUDLY':
                            $caseKey = studly_case($key);
                            break;
                        case 'UCFIRST':
                            $caseKey = ucfirst($key);
                            break;
                        default:
                            $caseKey = $key;
                    }

                    // Change key if needed
                    if ($caseKey != $key) {
                        unset($item[$key]);
                        $item[$caseKey] = $data;
                        $key = $caseKey;
                    }
                    // Handle nested arrays
                    if (is_array($data)) {
                        $this->array_change_key_case($item[$key], $case);
                    }

                }

                $collection[] = $item;

            }
        } else {
            foreach ($array as $key => $value) {
                switch (strtoupper($case)) {
                    case 'UPPER':
                        $caseKey = strtoupper($key);
                        break;
                    case 'LOWER':
                        $caseKey = strtolower($key);
                        break;
                    case 'CAMEL':
                        $caseKey = camel_case($key);
                        break;
                    case 'SNAKE':
                        $caseKey = snake_case($key);
                        break;
                    case 'STUDLY':
                        $caseKey = studly_case($key);
                        break;
                    case 'UCFIRST':
                        $caseKey = ucfirst($key);
                        break;
                    default:
                        $caseKey = $key;
                }
                // Change key if needed
                if ($caseKey != $key) {
                    unset($array[$key]);
                    $array[$caseKey] = $value;
                    $key = $caseKey;
                }
                // Handle nested arrays
                if (is_array($value)) {
                    $this->array_change_key_case($array[$key], $case);
                }
            }

            $collection = $array;

        }


        return $collection;
    }

}